package de.blioxxx.mc.aRecipe.tools;

import de.blioxxx.mc.aRecipe.game.general;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemPickaxe;

public class ItemMasterPickaxe extends ItemPickaxe {

    public ItemMasterPickaxe(ToolMaterial material){
        super(general.masterMaterial);
        setCreativeTab(CreativeTabs.tabTools);
        setTextureName("AdvancedRecipes:MasterPickaxe");
        setUnlocalizedName("MasterPickaxe");
    }

}
