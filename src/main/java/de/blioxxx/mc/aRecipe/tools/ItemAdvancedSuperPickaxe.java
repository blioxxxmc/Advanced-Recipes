package de.blioxxx.mc.aRecipe.tools;

import de.blioxxx.mc.aRecipe.game.general;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemPickaxe;
import net.minecraftforge.common.util.EnumHelper;

public class ItemAdvancedSuperPickaxe extends ItemPickaxe {

    public ItemAdvancedSuperPickaxe(ToolMaterial material){
        super(general.advancedSuperMaterial);
        setCreativeTab(CreativeTabs.tabTools);
        setTextureName("AdvancedRecipes:AdvancedSuperPickaxe");
        setUnlocalizedName("AdvancedSuperPickaxe");
    }

}
