package de.blioxxx.mc.aRecipe.tools;

import de.blioxxx.mc.aRecipe.game.general;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemPickaxe;
import net.minecraftforge.common.util.EnumHelper;

public class ItemAdvancedPickaxe extends ItemPickaxe {

    public ItemAdvancedPickaxe(ToolMaterial material){
        super(general.advancedMaterial);
        setCreativeTab(CreativeTabs.tabTools);
        setTextureName("AdvancedRecipes:AdvancedPickaxe");
        setUnlocalizedName("AdvancedPickaxe");
    }

}
