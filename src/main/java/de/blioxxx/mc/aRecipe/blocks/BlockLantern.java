package de.blioxxx.mc.aRecipe.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockLantern extends Block{

    public BlockLantern(Material material) {
        super(material);
        setCreativeTab(CreativeTabs.tabDecorations);
        setStepSound(Block.soundTypeGlass);
        setLightLevel(1.0F);
        setBlockTextureName("AdvancedRecipes:Lantern");
        setBlockName("BlockLantern");
    }
}
