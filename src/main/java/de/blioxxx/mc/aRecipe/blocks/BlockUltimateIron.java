package de.blioxxx.mc.aRecipe.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockUltimateIron extends Block{

    public BlockUltimateIron(Material material){
        super(material);
        setCreativeTab(CreativeTabs.tabBlock);
        setStepSound(Block.soundTypeMetal);
        setLightLevel(0.0F);
        setBlockTextureName("AdvancedRecipes:UltimateIron");
        setBlockName("BlockUltimateIron");
    }

}
