package de.blioxxx.mc.aRecipe;

import de.blioxxx.mc.aRecipe.game.recipes.Recipes;
import de.blioxxx.mc.aRecipe.game.recipes.smellingRecipes;
import de.blioxxx.mc.aRecipe.game.registerOwnItems;
import de.blioxxx.mc.aRecipe.sphen.registerSphenItems;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = "AdvancedRecipes", name = "AdvancedRecipes")
public class aRecipe {
    @Instance
    public static aRecipe instance = new aRecipe();

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent e){
        //Register Sphen Items
        new registerSphenItems().registerSphenItem();

        //Register Own Items
        new registerOwnItems().registerOwnItems();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent e){
        new Recipes().loadRecipes();
    }

    @Mod.EventHandler
    public  void postInit(FMLPostInitializationEvent e){

    }
}