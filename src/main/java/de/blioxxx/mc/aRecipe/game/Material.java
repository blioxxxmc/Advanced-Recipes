package de.blioxxx.mc.aRecipe.game;


import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraftforge.common.util.EnumHelper;

public class Material {
    //Sphen Materials
    public static Item.ToolMaterial sphenToolMaterial = EnumHelper.addToolMaterial("sphenToolMaterial", 3, 2800, 10.0F, 1.5F, 18);
    public static Item.ToolMaterial sphenSwordMaterial = EnumHelper.addToolMaterial("sphenSwordMaterial", 3, 2300, 5.5F, 6.5F, 18);
    public static ItemArmor.ArmorMaterial sphenArmorMaterial = EnumHelper.addArmorMaterial("sphenArmorMaterial", 350, new int[]{4, 9, 7, 4}, 18);
    public static net.minecraft.block.material.Material sphenBlockMaterial = net.minecraft.block.material.Material.iron;
}
