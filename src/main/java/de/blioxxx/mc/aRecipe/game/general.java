package de.blioxxx.mc.aRecipe.game;

import net.minecraft.item.Item;
import net.minecraftforge.common.util.EnumHelper;

public class general {
    //Tool Materials
    public static Item.ToolMaterial masterMaterial = EnumHelper.addToolMaterial("BXMaster", 3 , 3000, 9.0F, 4.0F, 24);
    public static Item.ToolMaterial advancedSuperMaterial = EnumHelper.addToolMaterial("AdvancedSuper", 3 , 550, 7.5F, 4.0F, 20);
    public static Item.ToolMaterial advancedMaterial = EnumHelper.addToolMaterial("Advanced", 2 , 400, 7.0F, 3.0F, 14);
}
