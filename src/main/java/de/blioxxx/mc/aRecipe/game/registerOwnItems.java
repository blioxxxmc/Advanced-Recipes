package de.blioxxx.mc.aRecipe.game;

import cpw.mods.fml.common.registry.GameRegistry;
import de.blioxxx.mc.aRecipe.blocks.BlockLantern;
import de.blioxxx.mc.aRecipe.blocks.BlockUltimateIron;
import de.blioxxx.mc.aRecipe.items.ItemFreshMeat;
import de.blioxxx.mc.aRecipe.tools.ItemAdvancedPickaxe;
import de.blioxxx.mc.aRecipe.tools.ItemAdvancedSuperPickaxe;
import de.blioxxx.mc.aRecipe.tools.ItemMasterPickaxe;
import net.minecraft.block.material.Material;


public class registerOwnItems {
    //Werkzeuge
    public static ItemAdvancedPickaxe advancedPickaxe;
    public static ItemAdvancedSuperPickaxe advancedSuperPickaxe;
    public static ItemMasterPickaxe masterPickaxe;

    //Blöcke
    public static BlockUltimateIron blockUltimateIron;
    public static BlockLantern blockLantern;

    //Items
    public static ItemFreshMeat itemFreshMeat;

    public void registerOwnItems(){
        registerOwnBlocks();
        registerOwnTools();
        registerOwnFood();
    }

    public void registerOwnBlocks(){
        //Register Blocks
        blockUltimateIron = new BlockUltimateIron(Material.iron);
        GameRegistry.registerBlock(blockUltimateIron, "blockUltimateIron");

        blockLantern = new BlockLantern(Material.glass);
        GameRegistry.registerBlock(blockLantern, "blockLantern");
    }

    public void registerOwnTools(){
        //Register Werkzeuge
        advancedPickaxe = new ItemAdvancedPickaxe(null);
        GameRegistry.registerItem(advancedPickaxe, "advancedPickaxe");

        advancedSuperPickaxe = new ItemAdvancedSuperPickaxe(null);
        GameRegistry.registerItem(advancedSuperPickaxe, "advancedSuperPickaxe");

        masterPickaxe = new ItemMasterPickaxe(null);
        GameRegistry.registerItem(masterPickaxe, "masterPickaxe");
    }

    public void registerOwnFood(){
        itemFreshMeat = new ItemFreshMeat();
        GameRegistry.registerItem(itemFreshMeat, "itemFreshMeat");
    }

}
