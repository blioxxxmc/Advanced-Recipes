package de.blioxxx.mc.aRecipe.game.recipes;

import de.blioxxx.mc.aRecipe.game.registerOwnItems;
import de.blioxxx.mc.aRecipe.sphen.*;
import de.blioxxx.mc.aRecipe.sphen.tools.*;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.GameRegistry;

public class Recipes {
    public void loadRecipes(){
        loadCraftRecipes();
        loadToolRecipes();
        loadOwnRecipes();
        loadSphenItemRecipes(registerSphenItems.itemSphenIngot,registerSphenItems.itemSphenSword,registerSphenItems.itemSphenAxe,registerSphenItems.itemSphenPickaxe,registerSphenItems.itemSphenShovel,registerSphenItems.itemSphenHoe);
        loadSphenComatRecipes(registerSphenItems.itemSphenIngot,registerSphenItems.sphenHelmet,registerSphenItems.sphenChestplate,registerSphenItems.sphenLaggings,registerSphenItems.sphenBoots);
        new smellingRecipes().loadSmellingRecipes();
    }

    public void loadCraftRecipes(){
        //Neue Diamanten
        GameRegistry.addRecipe(new ItemStack(Items.diamond, 4),new Object[]{
                "RRR",
                "CCC",
                "RRR",
                'R', Blocks.redstone_block,
                'C', Items.coal,
        });

        //New Iron
        GameRegistry.addRecipe(new ItemStack(Items.iron_ingot, 1), new Object[]{
                "   ",
                "CCC",
                "   ",
                'C', Items.coal
        });

        //Lapis to Redstone
        GameRegistry.addRecipe(new ItemStack(Items.redstone, 3), new Object[]{
                "   ",
                "LLL",
                "   ",
                'L', new ItemStack(Items.dye, 1, 4)
        });

        //Convert Diamonds to Iron
        GameRegistry.addShapelessRecipe(new ItemStack(Items.iron_ingot, 8), new Object[]{Items.diamond});

        //Convert Iron to Stone
        GameRegistry.addShapelessRecipe(new ItemStack(Blocks.stone, 16), new Object[]{Items.iron_ingot});
        GameRegistry.addShapelessRecipe(new ItemStack(Blocks.cobblestone, 32), new Object[]{Items.iron_pickaxe});
    }

    public void loadToolRecipes(){
        GameRegistry.addShapelessRecipe(new ItemStack(registerOwnItems.advancedPickaxe, 1), new Object[]{Items.iron_pickaxe,Items.stone_pickaxe});
        GameRegistry.addShapelessRecipe(new ItemStack(registerOwnItems.advancedSuperPickaxe, 1), new Object[]{Items.iron_pickaxe, Items.iron_pickaxe, Items.iron_pickaxe});
        GameRegistry.addRecipe(new ItemStack(registerOwnItems.masterPickaxe,1), new Object[]{
                "DDD",
                " P ",
                " P ",
                'D', Items.diamond,
                'P', Items.diamond_pickaxe
        });
    }

    public void loadOwnRecipes(){
        //Convert Iron Blocks to MultiIronBlocks
        GameRegistry.addRecipe(new ItemStack(new registerOwnItems().blockUltimateIron), new Object[]{
                "EEE",
                "EEE",
                "EEE",
                'E', Blocks.iron_block,
        });

        //Convert MultiIronBlocks to normal Iron Blocks
        GameRegistry.addShapelessRecipe(new ItemStack(Blocks.iron_block, 9), new Object[]{new registerOwnItems().blockUltimateIron});

        //Convert 2 Glowstone and 4 torches to Lantern Block
        GameRegistry.addShapelessRecipe(new ItemStack(new registerOwnItems().blockLantern), new Object[]{Items.glowstone_dust, Items.glowstone_dust});
        GameRegistry.addShapelessRecipe(new ItemStack(new registerOwnItems().blockLantern), new Object[]{Blocks.torch, Blocks.torch, Blocks.torch, Blocks.torch});

        //Craft Fresh Meat
        GameRegistry.addShapelessRecipe(new ItemStack(new registerOwnItems().itemFreshMeat), new Object[]{Items.rotten_flesh, Items.sugar, Items.sugar});
    }

    public void loadSphenItemRecipes(ItemSphenIngot itemSphenIngot, ItemSphenSword itemSphenSword, ItemSphenAxe itemSphenAxe, ItemSphenPickaxe itemSphenPickaxe, ItemSphenShovel itemSphenShovel, ItemSphenHoe itemSphenHoe){
        GameRegistry.addRecipe(new ItemStack(itemSphenIngot), new Object[]{
                "RRR",
                "RDR",
                "RRR",
                'R', Items.redstone,
                'D', Items.diamond,
        });

        GameRegistry.addRecipe(new ItemStack(itemSphenSword), new Object[]{
                " S ",
                " S ",
                " W ",
                'S', itemSphenIngot,
                'W', Items.stick,
        });

        GameRegistry.addRecipe(new ItemStack(itemSphenPickaxe), new Object[]{
                "SSS",
                " W ",
                " W ",
                'S', itemSphenIngot,
                'W', Items.stick,
        });

        GameRegistry.addRecipe(new ItemStack(itemSphenAxe), new Object[]{
                "SS ",
                "SW ",
                " W ",
                'S', itemSphenIngot,
                'W', Items.stick,
        });

        GameRegistry.addRecipe(new ItemStack(itemSphenShovel), new Object[]{
                " S ",
                " W ",
                " W ",
                'S', itemSphenIngot,
                'W', Items.stick,
        });

        GameRegistry.addRecipe(new ItemStack(itemSphenHoe), new Object[]{
                "SS ",
                " W ",
                " W ",
                'S', itemSphenIngot,
                'W', Items.stick,
        });
    }

    public void loadSphenComatRecipes(ItemSphenIngot itemSphenIngot,ItemArmor sphenHelmet,ItemArmor sphenChestplate,ItemArmor sphenLaggings,ItemArmor sphenBoots){
        //Load Sphen Comat
        GameRegistry.addRecipe(new ItemStack(sphenHelmet), new Object[]{
                "   ",
                "SSS",
                "S S",
                'S', itemSphenIngot,
        });

        GameRegistry.addRecipe(new ItemStack(sphenChestplate), new Object[]{
                "S S",
                "SSS",
                "SSS",
                'S', itemSphenIngot,
        });

        GameRegistry.addRecipe(new ItemStack(sphenLaggings), new Object[]{
                "SSS",
                "S S",
                "S S",
                'S', itemSphenIngot,
        });

        GameRegistry.addRecipe(new ItemStack(sphenBoots), new Object[]{
                "   ",
                "S S",
                "S S",
                'S', itemSphenIngot,
        });
    }
}
