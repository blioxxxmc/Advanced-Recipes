package de.blioxxx.mc.aRecipe.game.recipes;

import cpw.mods.fml.common.registry.GameRegistry;
import de.blioxxx.mc.aRecipe.game.registerOwnItems;
import de.blioxxx.mc.aRecipe.items.ItemFreshMeat;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class smellingRecipes {
    public void loadSmellingRecipes(){
        GameRegistry.addSmelting(Items.rotten_flesh, new ItemStack(Items.leather), 7.0F);
        GameRegistry.addSmelting(registerOwnItems.itemFreshMeat, new ItemStack(Items.cooked_beef), 5.0F);
    }
}
