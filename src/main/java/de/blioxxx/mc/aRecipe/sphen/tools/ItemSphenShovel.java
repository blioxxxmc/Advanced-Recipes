package de.blioxxx.mc.aRecipe.sphen.tools;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemSpade;

public class ItemSphenShovel extends ItemSpade {

    public ItemSphenShovel(ToolMaterial material){
        super(material);
        setCreativeTab(CreativeTabs.tabTools);
        setTextureName("AdvancedRecipes:SphenShovel");
        setUnlocalizedName("SphenShovel");
    }

}
