package de.blioxxx.mc.aRecipe.sphen.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class SphenBlock extends Block {
    public SphenBlock(Material material){
        super(material);
        setCreativeTab(CreativeTabs.tabBlock);
        setStepSound(Block.soundTypeMetal);
        setLightLevel(0.0F);
        setBlockTextureName("AdvancedRecipes:SphenBlock");
        setBlockName("SphenBlock");
    }
}
