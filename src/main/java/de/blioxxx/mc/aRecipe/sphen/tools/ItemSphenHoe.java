package de.blioxxx.mc.aRecipe.sphen.tools;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemHoe;

public class ItemSphenHoe extends ItemHoe {

    public ItemSphenHoe(ToolMaterial material){
        super(material);
        setCreativeTab(CreativeTabs.tabTools);
        setTextureName("AdvancedRecipes:SphenHoe");
        setUnlocalizedName("SphenHoe");
    }

}
