package de.blioxxx.mc.aRecipe.sphen;

import cpw.mods.fml.common.registry.GameRegistry;
import de.blioxxx.mc.aRecipe.game.Material;
import de.blioxxx.mc.aRecipe.sphen.blocks.SphenBlock;
import de.blioxxx.mc.aRecipe.sphen.combat.SphenArmor;
import de.blioxxx.mc.aRecipe.sphen.tools.*;
import net.minecraft.item.ItemArmor;

public class
registerSphenItems {
    //Sphen Items
    public static ItemSphenIngot itemSphenIngot;
    public static ItemSphenSword itemSphenSword;
    public static ItemSphenPickaxe itemSphenPickaxe;
    public static ItemSphenAxe itemSphenAxe;
    public static ItemSphenShovel itemSphenShovel;
    public static ItemSphenHoe itemSphenHoe;

    //Sphen Blocks
    /*public static SphenBlock sphenBlock;*/

    //Sphen Combat
    public static ItemArmor sphenHelmet = new SphenArmor(Material.sphenArmorMaterial, 0 , 0);
    public static ItemArmor sphenChestplate = new SphenArmor(Material.sphenArmorMaterial, 0 , 1);
    public static ItemArmor sphenLaggings = new SphenArmor(Material.sphenArmorMaterial, 0 , 2);
    public static ItemArmor sphenBoots = new SphenArmor(Material.sphenArmorMaterial, 0 , 3);


    //Methodes
    public void registerSphenItem(){
        //Register SphenItems
        itemSphenIngot = new ItemSphenIngot();
        GameRegistry.registerItem(itemSphenIngot, "itemSphenIngot");

        itemSphenSword = new ItemSphenSword(new Material().sphenSwordMaterial);
        GameRegistry.registerItem(itemSphenSword, "itemSphenSword");

        itemSphenPickaxe = new ItemSphenPickaxe(new Material().sphenToolMaterial);
        GameRegistry.registerItem(itemSphenPickaxe, "itemSphenPickaxe");

        itemSphenAxe = new ItemSphenAxe(new Material().sphenToolMaterial);
        GameRegistry.registerItem(itemSphenAxe, "itemSphenAxe");

        itemSphenShovel = new ItemSphenShovel(new Material().sphenToolMaterial);
        GameRegistry.registerItem(itemSphenShovel, "itemSphenShovel");

        itemSphenHoe = new ItemSphenHoe(new Material().sphenToolMaterial);
        GameRegistry.registerItem(itemSphenHoe, "itemSphenHoe");

        //Register Sphen Blocks
/*        sphenBlock = new SphenBlock(new Material().sphenBlockMaterial);
        GameRegistry.registerBlock(sphenBlock, "sphenBlock");*/

        //Register Sphen Combat Items
        GameRegistry.registerItem(sphenHelmet, "sphenHelmet");
        GameRegistry.registerItem(sphenChestplate, "sphenChestplate");
        GameRegistry.registerItem(sphenLaggings, "sphenLaggings");
        GameRegistry.registerItem(sphenBoots, "sphenBoots");
    }
}
