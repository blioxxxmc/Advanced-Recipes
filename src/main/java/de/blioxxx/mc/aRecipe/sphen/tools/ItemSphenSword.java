package de.blioxxx.mc.aRecipe.sphen.tools;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemSword;

public class ItemSphenSword extends ItemSword{

    public ItemSphenSword(ToolMaterial material) {
        super(material);
        setCreativeTab(CreativeTabs.tabCombat);
        setTextureName("AdvancedRecipes:SphenSword");
        setUnlocalizedName("SphenSword");
    }

}
