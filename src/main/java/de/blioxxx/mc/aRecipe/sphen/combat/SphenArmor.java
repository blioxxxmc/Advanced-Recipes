package de.blioxxx.mc.aRecipe.sphen.combat;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class SphenArmor extends ItemArmor{

    public SphenArmor(ArmorMaterial material, int renderIndex, int armorType) {
        super(material, renderIndex, armorType);

        switch (armorType){
            case 0:
                setUnlocalizedName("SphenHelmet");
                setTextureName("AdvancedRecipes:SphenHelmet");
                break;
            case 1:
                setUnlocalizedName("SphenChestplate");
                setTextureName("AdvancedRecipes:SphenChestplate");
                break;
            case 2:
                setUnlocalizedName("SphenLeggings");
                setTextureName("AdvancedRecipes:SphenLeggings");
                break;
            case 3:
                setUnlocalizedName("SphenBoots");
                setTextureName("AdvancedRecipes:SphenBoots");
                break;

        }

        setCreativeTab(CreativeTabs.tabCombat);
    }

    @Override
    public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
        if(slot == 0 || slot == 1 || slot == 3){
            return "advancedrecipes:textures/models/armor/sphen_layer_1.png";
        }else if (slot == 2){
            return "advancedrecipes:textures/models/armor/sphen_layer_2.png";
        }else{
            return null;
        }
    }
}
