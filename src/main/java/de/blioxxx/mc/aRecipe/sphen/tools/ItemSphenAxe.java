package de.blioxxx.mc.aRecipe.sphen.tools;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemPickaxe;

public class ItemSphenAxe extends ItemAxe {

    public ItemSphenAxe(ToolMaterial material){
        super(material);
        setCreativeTab(CreativeTabs.tabTools);
        setTextureName("AdvancedRecipes:SphenAxe");
        setUnlocalizedName("SphenAxe");
    }

}
