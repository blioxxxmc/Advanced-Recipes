package de.blioxxx.mc.aRecipe.sphen.tools;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemPickaxe;
import net.minecraftforge.common.util.EnumHelper;

public class ItemSphenPickaxe extends ItemPickaxe {

    public ItemSphenPickaxe(ToolMaterial material){
        super(material);
        setCreativeTab(CreativeTabs.tabTools);
        setTextureName("AdvancedRecipes:SphenPickaxe");
        setUnlocalizedName("SphenPickaxe");
    }

}
