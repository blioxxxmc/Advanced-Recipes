package de.blioxxx.mc.aRecipe.sphen;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemSphenIngot extends Item{

    public ItemSphenIngot(){
        setCreativeTab(CreativeTabs.tabMaterials);
        setMaxStackSize(64);
        setUnlocalizedName("ItemSphenIngot");
        setTextureName("AdvancedRecipes:SphenIngot");
        setUnlocalizedName("SphenIngot");
    }

}
