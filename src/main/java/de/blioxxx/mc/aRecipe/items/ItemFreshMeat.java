package de.blioxxx.mc.aRecipe.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemFood;

public class ItemFreshMeat extends ItemFood{

    //int healAmout, float saturationFactor, boolean canEatByWolf
    public ItemFreshMeat() {
        //healAmout, saturationFactor, canEatByWolf
        super(3, 2.0F, true);
        setCreativeTab(CreativeTabs.tabFood);
        setMaxStackSize(64);
        setTextureName("AdvancedRecipes:FreshMeat");
        setUnlocalizedName("ItemFreshMeat");
    }
}
